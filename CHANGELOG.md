Changelog Master -> Test ...
=========
# 12-5-2022
### NLU & Stories
* New intent
    - bijshrijven_hoe \
    voorbeeld: kan ik mijn kind bijschrijven in mijn paspoort
* Merged intent
    - product_paspoort_kind_bijschrijven to bijschrijven_hoe
* New entities
    - rijbewijscode \
    voorbeeld: Code 95 bijschrijven
    - rijbewijscategorie = product \
    voorbeeld: Ik wil mijn vrachtwagenrijbewijs aanvragen
* Change intent name in stories
    - product_paspoort_kind_bijschrijven to bijschrijven_hoe

# 10-8-2021 - 22-9-2021
### NLU & stories
* Multi-intents
    - afspraakmaken+hoe_doorgeven \
    voorbeeld: Ik wil een afspraak maken om mijn verhuizing door te geven.
    - reisdocument verlopen+afspraakmaken \
    voorbeeld: Mijn paspoort is verlopen, hoe kan ik een afspraak maken?
    - out_of_scope+hoe_aanvragen \
    voorbeeld: Hoe vraag ik een rolstoel aan?
    - hoe_aanvragen+digid \
    voorbeeld: Hoe vraag ik een DigiD aan?
* Rijbewijs uitbreiding
    - hier + rijbewijs \
    voorbeeld: Ik wil mijn rijbewijs hier verlengen
    - BSN + rijbewijs \
    voorbeeld: Ik kan geen BSN vinden op mijn rijbewijs
    - wie + rijbewijs \
    voorbeeld: Wie kan een rijbewijs aanvragen?
    - wanneer aanvragen + rijbewijs \
    voorbeeld: Wanneer moet ik een rijbewijs aanvragen?
    - wanneer nodig + rijbewijs \
    voorbeeld: Wanneer heb ik een rijbewijs nodig?
    - hoelang + rijbewijs \
    voorbeeld: Hoelang duurt de aanvraag van een rijbewijs?
    - hoelang geldig + rijbewijs \
    voorbeeld: Hoelang is een rijbewijs geldig?
    - wat/watnodig/hoe + rijbewijs \
    voorbeeld: Wat is dat voor een rijbewijs / Wat heb ik nodig voor een rijbewijs / Hoe gaat dat voor een rijbewijs?
    - kapot + rijbewijs \
    voorbeeld: Mijn rijbewijs is kapot
    - rijbewijs aanvragen zonder inschrijving \
    voorbeeld: Ik ben dakloos en wil een rijbewijs aanvragen
    - internationaal rijbewijs aanvragen \
    voorbeeld: Ik wil een internationaal rijbewijs aanvragen
    - waar + internationaal rijbewijs \
    voorbeeld: Waar koop ik een internationaal rijbewijs?
    - aangiftedoen + rijbewijs \
    voorbeeld: Moet ik aangifte doen van mijn verloren rijbewijs?
    - bezorgmogelijkheid + rijbewijs \
    voorbeeld: Kan mijn rijbewijs ook bezorgd worden?
    - watmeenemen + rijbewijs \
    voorbeeld: Wat moet ik meenemen voor mijn nieuwe rijbewijs?
    - Update in alle utter_ask_products voor de optie rijbewijs
    - Aanpassing in de welkomstboodschap van Gem
    - Stories gemaakt zodat action_koepelproduct ook voor rijbewijs werkt

* Overig
    - Intent huisvestingsvergunning \
    voorbeeld: Heb ik een huisvestingsvergunning nodig?

### Actions/Forms/Policies
* Language detection policy: vreemde talen worden herkend en daarop wordt geanticipeerd
* Uitzondering voor intent "english" in de action_check_municipality gemaakt

### Other
* End-to-end test update
* Livecom integratie met router
* De-escalatie van livechat terug naar Gem
* Widget meta-data fix voor lokaal testen


# 10-8-2021 - 10-8-2021
### NLU & stories
* #1698: multi intent reisdocument verlopen + afspraak maken
voorbeeld: Mijn id-kaart is vorige week verlopen, kan ik een afspraak maken voor een nieuwe?
* #2166: hoe_aanvragen + digid
voorbeeld: Hoe vraag ik een digid aan?
### NLU(DIET) config
* Random_seed is ingesteld om reproduceerbare resultaten te krijgen
* number_of_transformer_layers is aangepast naar 4
* Default hyperparameters zijn ingesteld voor fine-tuning

# 3-7-2020 - 10-8-2021
### NLU & stories
* Nieuwe stories + nlu voor rijlesnemen_minderjarig
voorbeeld: mag ik onder begeleiding autorijden?
* Multi-intents
    - hoe_aanvragen+spoed \
    voorbeeld: Wat moet ik doen om een id-kaart met spoed aan te vragen?
    - afspraakmaken+hoe_aanvragen \
    voorbeeld: Ik wil graag een afspraak maken voor het aanvragen van een paspoort
    - afspraaknodig+productgroep_reisdocumenten_afhalen_hoe \
    voorbeeld: Moet ik een afspraak maken voor het afhalen van mijn id-kaart
    - afspraaknodig+hoe_aanvragen \
    voorbeeld: heb ik een afspraak nodig om mijn paspoort aan te vragen
    - afspraakmaken+productgroep_reisdocumenten_afhalen_hoe \
    voorbeeld: Ik wil woensdag een afspraak maken om mijn paspoort op te halen
    - inform_leeftijd+rijlesnemen_minderjarig \
    voorbeeld: Ik ben nog geen 18. Mag ik al rijles nemen?
    - watregelen+producten_algemeen \
    voorbeeld: Wat zou ik allemaal moeten doen als ik ga verhuizen

# 6-7-2020 - 20-7-2021
### NLU & stories
* #2069 Nieuwe stories + NLU voor Rijbewijs
    - hoe_aanvragen
    - hoe_aanvragen_1ekeer
    - hoe_verlengen
    - hoe_afhalen
* #2066 Nieuwe entity + story voor gezondheidsverklaring
* Ask_for_human ook naadloos naar livechat

### Other
* #2127 Fix voor escalatie naar livecom

# 16-2-2020 - 2-3-2021
### NLU & stories
* Intent 'overig_verkiezingen' toegevoegd.
* Algehele herstructurering van NLU.
* #1469: Verdiepingsintent 'schriftelijk' toegevoegd.
* #1380: Nieuwe intent 'verhuismelding_huurwoning' toegevoegd. 
* #1171: Algehele intent 'bevestiging' gemaakt en bestaande intents daarin verwerkt.
* Intent 'productgroep_reisdocumenten_kwijt' samengevoegd met intent 'kwijt'.
* #1447: Out-of-scope product en intent 'afval' toegevoegd.
* #1517: Nieuwe intent 'productgroep_reisdocumenten_aanvragen_anderegemeente' toegevoegd.

### Actions/Forms/Policies
* #1389: Form_when_verhuismelding aangepast zodat user input 'weet niet' weer werkt.

### Other
* Diverse toegankelijkheidsaanpassingen verwerkt in de widget.  
* Custom language tool toegevoegd voor NLU prediction
* Entities 'niet nederlander', 'noodpaspoort', 'tweede paspoort', 'adresonderzoek' en 'afspraak' toegevoegd en doorgevoerd in NLU en domain.

# 26-1-2020 - 09-2-2021
### NLU & stories
* Intent (en verdiepingsintent) 'luktniet' toegevoegd.
* Intent 'briefadres' toegevoegd.
* Out-of-scope product 'naturalisatie' toegevoegd.
* #1402: Action 'envoor' wordt afgetrapt na inform_municipality bij hoe verhuizing doorgeven stories.
* #413: Combinatie van intent 'email' en entiteit 'verhuismelding' in stories.
* #1307: Intent 'productgroep_reisdocumenten_vanafleeftijd' toegevoegd.
* #1413: Intent 'product_verhuismelding_wanneerofficieel' toegevoegd.
* #1394: Intent 'product_verhuismelding_poststoppen' toegevoegd.

### Actions/Forms/Policies
* Action 'setspoed' toegevoegd, zodat gebruikers entiteit 'spoed' kunnen gebruiken in alle intents.
* #1394: Aanpassing in action_bedoelde zodat intent 'medewerker_nodig' wordt afgetrapt na keyboard-input 'iets anders'.

### Other
* Router vertaal API Deepl toegevoegd.

# 12-1-2020 - 19-1-2021
### NLU & stories
* #1285: Entiteit 'land' toegevoegd aan producten_algemeen stories
* #1104: Stories voor gevonden voorwerpen toegevoegd
* #1256: Entiteit 'land' toegevoegd aan stories voor intent 'English'
* #1176: Entiteit 'watmeenemen' toegevoegd aan stories voor intent 'product_paspoort_aanvragen_watmeenemen'
* #1074; Entiteit-waarde 'onderhuurder' toegevoegd aan intent 'product_verhuismelding_uitschrijven'
* Story gemaakt voor niet-aangesloten varian van 'product_verhuismelding_uitschrijven_ex aangesloten'
* #1257: Story aangemaakt voor 'producten_algemeen' i.c.m. entiteiten product en namens.
* #1292: Stories aangepast voor 'productgroep_reisdocumenten_afhalen_hoe'
* Belasting stories, NLU en utters toegevoegd


### Actions/Forms/Policies
* #1294: Fix in form_verhuismelding_when bij geven ongeldige verhuisdatum

### Other
* Widget voor Utrecht werkt nu ook op mobile devices
* Pasfoto: tijdelijke aanvulling 'waar maken in coronatijd'
* Afbeeldingen worden geschaald zodat ze in de widget passen

# 15-12-2020 - 5-1-2021
### NLU & stories
* 486: 'utter_la_product_verhuismelding_hoe' verwijderd zodat verdiepingsmechanisme niet aftrapt
* Meerdere trainingphrases toegevoegd en NLU heringedeeld
* #1259: button toegevoegd aan utter_ask_korterevraag
* #1253: Nieuwe stories toegevoegd voor intent overig_foto_eisen
* intent 'gevonden_voorwerpen' en lokale utter toegevoegd

### Actions/Forms/Policies
* #1155: Fix in calculeermechanisme verhuisformulier
* #1097: 'utter_default' en 'utter_teamtest' toegevoegd aan exclude-lijst voor verdiepingpolicy
* #1198: Check toegevoegd voor de action_default_fallback_core in de action_check_askforhuman

### Other
* Lookup files voor entities toegevoegd
* Metadata widget toegevoegd; zien op welke pagina een chat gestart is

# 09-12-2020
### NLU & stories
* Nieuwe entity-waarde 'ex'

### Actions/Forms/Policies
* #1156: Utter_lo_burgemeester toegevoegd en action_burgemeester aangepast
* #1146: Aanpassing in form_when_verhuismelding zodat '2021' wordt herkend
* #147: Aanpassing in action_check_livechat zodat meta-data gevonden kan worden om livechat aan te bieden

### Other
* #1152: Pipeline-component toegevoegd om (te) lange user-input te herkennen. Indien langer dan 25 worden, wordt de gebruiker gevraagd om zijn vraag korter te formuleren
* NLU 'zekerheidsdrempel' aangepast van 0.6 naar 0.7
* Compenent toegevoegd dat het mogelijk maakt om externe bots/livechat te testen via de router.


# 02-12-2020
### NLU & stories
* #1024: Intent 'niets' toegevoegd aan stories na action_helpmore
* #1064: Landelijke utters lokaal gemaakt
* #1062: Stories aangepast zodat lokaal antwoord mogelijk wordt
* Nieuwe intent 'help' toegevoegd aan NLU en stories
* Nieuwe entity waardes 'zus' en 'broer' toegevoegd
* #1066 #1073: Nieuwe intent 'email' toegevoegd aan NLU en stories

### Actions/Forms/Policies
* #1123: Aanpassing in action_default_ask_affirmation: kijkt na laatste 5 events en geeft escalatie

### Other
* #922: Pagina 'adresonderzoek' toegevoegd aan local.yml
* Nieuwe end-2-end testdata toegevoegd


# 25-11-2020
### NLU & stories
* #922: Stories en utters voor nieuwe intents 'adresonderzoek_ontvangen' & 'adresonderzoek_aanvragen'
* #1054: Nieuwe verdiepingsintent 'watregelen' toegevoegd en toegevoegd aan relevante stories
* #979: Nieuwe story voor intent 'medewerker_nodig' i.c.m. product 'paspoort'
* #1022: Stories en utters voor nieuwe intent 'inschrijven_2adressen'
* #846: Nieuwe stories voor entity-waarde 'gezinsleden'
* #165: Entity 'municipality' wordt genegeerd bij de intent 'hoelang'
* #1060: Nieuwe entity-waarde 'zus' 
* #1036: Stories en utters voor nieuwe intent 'houdjemond'

### Actions/Forms/Policies
* #1025: Aanpassing in action_sc_resultaat voor voice kanaal
* #1083: Action_check_askforhuman toegevoegd om intent ask_for_human correct af te trappen na fallback

### Other


# 18-11-2020
### NLU & stories
* #1037: Stories voor 'ja' en 'nee' response na de action_default_fallback.
* #1006: Story voor de situatie dat iemand direct naar een medewerker vraagt.
* #1010: Nieuwe NLU en stories voor intent 'productgroep_reisdocumenten_overledene'.

### Actions/Forms/Policies

### Other
* Nieuwe entiteitwaarde 'gezinsleden' aangemaakt voor entiteit 'namens'.
* Connectie tussen livechat Dordrecht (Livecom) en Gem gerealiseerd.
* #1026: Oplossing voor spelfouten waarin 3 achtereenvolgende gelijke karakters staan.
* Nieuwe end-2-end testcase voor de situatie dat de livechat niet is aangesloten.

# 11-11-2020
### NLU & stories
* #834: Button 'medewerker spreken' toegevoegd aan 'utter_la_feedback_ontevreden_intro'
> Als iemand aangeeft ontevreden te zijn, geeft Gem nu ook de optie om een medewerker te spreken, bv. via livechat. 
* Buttons toegevoegd aan 'utter_la_medewerker_nodig_zonderlivechat'
> Als er geen livechat beschikbaar is, biedt Gem nu buttons aan om contactgegevens te krijgen of om door te gaan met Gem. 
* #1011: NLU toegevoegd t.b.v. intentherkenning
> Vraagherkenning verbeterd over veiligheid en privacy
* #712: NLU toegevoegd t.b.v. intentherkenning
> Vraagherkenning verbeterd over doorvragen nadat Gem de periode voor doorgeven verhuismelding heeft gegeven
* #906: NLU toegevoegd t.b.v. intentherkenning
> Vraagherkenning verbeterd over 'wat heb ik nodig' bij doorgeven verhuismelding
* #928 #895: Fix in out-of-scope stories voor getrainde producten
> Gem weet over welke onderwerpen ze geen antwoord kan geven. Ze moet dan aangeven dat ze daarmee nog niet kan helpen, en aanbieden om met een medewerker te spreken. Dat ging niet altijd goed. Dat is nu gerepareerd. 
* #835: Nieuwe stories product + gemeentenaam
> Gem kan nu ook vragen herkennen zoals 'paspoort Dordrecht' en 'verhuizing Utrecht'.
* #1012: 'Waarde' verwijderd als synoniem voor gemeente Reimerswaal
> Als iemand het woord 'waarde' gebruikte, dacht Gem dat het om gemeente Reimerswaal ging. Dat is nu gerepareerd.
* #908: "ee" verwijderd als synoniem voor Noardeast-Fryslan
> Vergelijkbaar: bij 'ee' dacht Gem dat het om de gemeente Noardeast-Fryslan ging. Dat is nu gerepareerd. 
* #819: Update in utter inhoud samenwerkende catalogi voice
> Bij voice moet het verwijzen naar samenwerkende catalogi (voor niet-aangesloten gemeenten en/of niet-getrainde producten) anders dan bij screen, omdat het voorlezen van een lijst met hyperlinks niet zinnig is bij voice. 
* #965: Utter "utter_la_productgroep_reisdocumenten_aanvragen_wanneerklaar" verwijderd en vervangen met reeds bestaande utter
> Opschoonwerk m.b.t. de vraag 'wanneer is mijn paspoort / id-kaart klaar'
* #917: Stories en utters voor nieuwe intent 'english'
> Als iemand vraagt 'do you speak English', kan Gem in het Engels antwoorden dat ze dat niet kan, en verwijzen naar een medewerker via livechat. 
* #535 #795: Nieuwe entiteit 'identiteitsbewijs' toegevoegd
> Soms vragen mensen naar een identiteitsbewijs. Dit kan een paspoort zijn, of bijvoorbeeld een ID-kaart of rijbewijs. Hier moet Gem dus doorvragen of aangepast antwoord geven. 
* #918: Stories en utters voor nieuwe intent 'overig_storing'
> Gem kan nu herkennen wanneer mensen aangeven dat er een storing op de website is, en hier adequaat op antwoorden. 
* #922: Stories en utters voor nieuwe intent 'product_verhuismelding_adresonderzoek'
> Gem kan nu vragen over adresonderzoek herkennen en beantwoorden. 
* #764: Stories en utters voor nieuwe intent 'product_verhuismelding_samenwonen_watregelen'
> Gem kan nu vragen over samenwonen herkennen en beantwoorden. 
* #904: Stories en utters voor nieuwe intent 'kind_leeftijd_bevestiging'
> Soms vragen mensen door als ze horen dat hun kind mee moet bij het aanvragen van een paspoort. Bijvoorbeeld 'maar mijn kind is pas 3 weken'. Dan bevestigt Gem dat het kind mee moet, alsmede geldige pasfoto en eventuele oude paspoort/id. 
* 'utter_lo_livechatnietaanwezig' toegevoegd


### Actions/Forms/Policies
* #1000: Aanpassing in action_default_ask_affirmation: bij een user-input langer dan 300 tekens wordt 'utter_la_korterevraag' gestuurd
> Soms typen mensen lange verhalen in, soms met meerdere zinnen achter elkaar. Dat kan een virtuele assistent niet goed herkennen. Daarom vraagt Gem in zo'n geval of iemand de vraag korter kan stellen. 
* Aanpassing in action_check_livechat: geeft nu slotwaardes 'gesloten', 'open', en 'niet_aangesloten'
> Bij gemeenten met gekoppelde livechat kan deze 'open' zijn of 'gesloten'. Er is nu ook een variant 'niet_aangesloten' voor wanneer een gemeente wel livechat heeft, maar deze nog niet gekoppeld is aan Gem. 

### Other
* Data augmentation uitgeschakeld. Dit zorgt voor een snellere training én maakt het makkelijker om stories te maken die starten met een utter in plaats van een intent.
> We willen Gem kunnen trainen dat als een bepaalde vraag komt na een specifiek antwoord van Gem, er een ander antwoord komt dan wanneer die vraag 'los' gesteld wordt. Dit is nu mogelijk. Leuke bijvangst is dat het trainen van Gem nu ook sneller gaat. 
* #756: (Voice) Toevoeging van voice-specifieke stories, zie in issue.
> Bij voice moeten soms net iets andere antwoorden gegeven worden dan bij screen. 
* 'Chatbot' is vervangen door 'digitale hulp' of 'digitale assistent'
> Het woord 'chatbot' is lastig en dekt de lading niet meer. We spreken nu van 'digitale hulp' of 'digitale assistent'. 
* Slot 'feature_livechat' veranderd naar categorical en waardes gewijzigd naar 'gesloten', 'open', en 'niet_aangesloten'
> Bij gemeenten met gekoppelde livechat kan deze 'open' zijn of 'gesloten'. Er is nu ook een variant 'niet_aangesloten' voor wanneer een gemeente wel livechat heeft, maar deze nog niet gekoppeld is aan Gem. 


# 04-11-2020
### NLU & stories
* #538: Nieuwe stories voor BSN nummer toegevoegd.

### Actions/Forms/Policies

### Other
* Privacy statement voor de gemeente Tilburg werkend gemaakt.
* Rasa staging geupdate naar versie 1.10.12.
* 'Chatbot' is vervangen door 'digitale hulp'
* #819: Voice: sc_resultaat stuurt een link naar zoekresultatenpagina

# 28-10-2020
### NLU & stories
* #928: Getraind product 'verhuismelding' toegevoegd aan out-of-scope stories.
* #825: Stories, NLU en utters verwijderd voor vluchtelingen- en vreemdelingenpaspoort.

### Actions/Forms/Policies

### Other
* End2End test training data uitgebreid.
* Duckling entity 'numbers' verwijderd.
* Voice: SSML fix in action_ask_affirmation & webhook overgezet naar productie-omgeving.

# 21-10-2020
### NLU & stories
* #825: Nieuwe stories en utters voor vluchtelingenpaspoort.
* #824: NLU & stories toegevoegd voor nieuwe intent 'productgroep_reisdocumenten_verlengen_herinnering'.


### Actions/Forms/Policies
* #870: Als de laatste action een check_municipality was, en er komen suggesties, daar altijd 'de naam van je gemeente' bij laten staan.
* #748: Bug fix: Als 'product_verhuismelding_doorgeven_wanneer' in suggesties stond en user-input bevatte een datum liep het verhuisformulier fout.


### Other


# 07-010-2020 + 14-10-2020
### NLU & stories
* #386: Gebruikers kunnen ook met een tijd antwoorden op de vraag 'Heb je je paspoort vandaag nog nodig?'.
* #781: NLU & stories toegevoegd voor nieuwe intent 'verhuismelding_bijouders'.
* #835: Nieuwe stories en utters voor intent 'producten_algemeen'.
* #353: Intents samengevoegd naar: 'reisdocumenten_kindmee'.
* #869: Nieuwe stories en utters voor intent 'openingstijden_livechat'.

### Actions/Forms/Policies
* action_default_ask_affirmation (suggesties) stuurt de suggestie-buttons door naar het voice kanaal.


### Other
* Voice: SSML (paragraph tags voor zinnen + break times voor komma's) wordt automatisch toegevoegd aan kanaal-generieke responses die naar de Google Assistant worden verstuurd.
* Voice: kanaal-specifieke responses die overbodig werden door bovenstaande toevoeging zijn verwijderd uit responses.yml.

# 30-09-2020
### NLU & stories
* Entities gemaakt voor postcodes, telefoonnnummers en emails. 
* #756: Helft voice stories toegevoegd.
* Verhuisproces stories toegevoegd (Zuiddrecht)

### Actions/Forms/Policies
* action_verhuisproces voor demo Zuiddrecht.

### Other
* Drechtsteden toegevoegd aan lokale antwoorden.
* #517: Utters aangepast zodat verdiepingspolicty wordt gestart.
* #514: Check op parameter 'type' vanuit buttons in responses.yml binnen de ga_connector, om een link te herkennen.


# 23-09-2020
### NLU & stories
* #647: In NLU trainingszinnen het voorbeeld 'belasting kwijtschelding' i.p.v. 'belasting-kwijtschelding' als product verwijderd.
> Belasting kwijtschelding wordt nu beter herkend
* Intent 'nieuwe' aan stories toegevoegd.
> Als iemand aangeeft 'een nieuwe' te willen of nodig te hebben, komt diegene nu nog vaker bij het juiste antwoord uit. 

### Actions/Forms/Policies
* #841: Fix in actions 'default_fallback' en 'default_fallback_core' om te voorkomen dat beide worden afgetrapt.
> Gem gaf soms 2x achter elkaar aan de klant niet te kunnen helpen. Nu geeft Gem dat nog maar 1x aan. 
* Voice: Aanpassing in 'action_helpmore' zodat 'utter_la_helpmore' ook op het voice kanaal wordt verstuurd.
> In google home moet elk antwoord gevolgd worden door de vraag of gem nog ergens mee kan helpen. 

### Other
* Rasa widget update naar versie 0.11.7.
> We blijven actueel :)
* Aansluiten.md file toegevoegd als stappenplan voor gemeenten die willen aansluiten.
> We hebben een stappenplan gemaakt voor gemeenten die willen aansluiten. Deze werken we steeds bij naar de meest recente ontwikkelingen. 

# 09-09-2020 + 16-09-2020
### NLU & stories
* #823: NLU & stories toegevoegd voor nieuwe intent 'hoe_ontstaan'
> Gem kan nu antwoord geven op de vraag "hoe ben jij ontstaan"
* #723: NLU & stories toegevoegd voor nieuwe intent 'overalhetzelfde'.
> Gem kan nu antwoord geven op de (door)vraag 'is dat overal hetzelfde' binnen een dialoog over de kosten van een paspoort of ID-kaart. 
* #756: Nieuwe stories & responses specifiek voor voice.
> Sommige antwoorden moeten anders zijn in voice dan in een chatvenster binnen een website. Waar nodig/behulpzaam hebben we de antwoorden aangepast zodat ze beter klinken in voice. Waar nodig hebben we ook de opvolgende stories aangevuld, bijvoorbeeld als het antwoord 'ja/nee'  wordt in voice, in plaats van het aanklikken van een button in het chatvenster. 
* #386: 'Time' entiteit toegevoegd als antwoord op 'heb je het vandaag nog nodig' bij aanvragen reisdocument met spoed.
> Klanten blijken regelmatig een tijdindicatie (bv. 'vandaag' of 'morgen') te geven als Gem vraagt of ze hun spoed paspoort of id-kaart vandaag nog nodig hebben. Gem raakte daarvan in de war; nu niet meer. 
* Nieuwe stories voor intents 'telefonisch-doorgeven' & 'product_klacht_indienen'
> Gem kan nu nog beter antwoord geven op de vragen "Kan ik dat telefonisch doorgeven" (bv. verhuizing) en als de klant aangeeft een klacht in te willen dienen. 

### Actions/Forms/Policies
* Aanpassing in 'action_check_municipality', zodat deze controleert of een gemeente is geregistreerd in het antwoord CMS of NLX.
> Gemeenten die aansluiten kunnen gebruik maken van het kant-en-klare instap-cms, óf hun eigen CMS koppelen via een API via de NLX. Gem moet weten op welke manier de gemeente aangesloten, om te kunnen weten waar het (lokale) antwoord vandaan moet komen. 

### Other
* #756 + #514: Buttons + Linkout-buttons toegevoegd aan voice kanaal. 
> Sommige voice gebruikers hebben een scherm in hun device. Voor die klanten is het fijn als buttons en links wél getoond worden. Daar is nu een oplossing voor. 
* Aanpassing in NLG zodat buttons kanaal-specifiek worden.
> De buttons die getoond worden moeten soms verschillend zijn per kanaal (bv. chatwidget vs. voice met scherm). Dit kan nu. 
* Dynamische kosten door middel van hyperlinks in local.yml.
> Naast hyperlinks kunnen leges (bv. paspoort kosten) nu ook centraal beheerd worden en dynamisch in verschillende utters aangeroepen worden via <codes>
* Utrecht & Tilburg toegevoegd als aangesloten gemeenten. 
> Gem kan nu ook antwoord geven voor bewoners van Utrecht en Tilburg. 
* Rasa versie upgrade naar 1.10.12.
> We blijven up to date :)
* Fix in text processor zodat datums als '1-1-2020' goed worden begrepen door Duckling.
> Nóg betere datumherkenning

# 02-09-2020
### NLU & stories
* #712: Aanpassing in stories. Intents 'datum_bevestiging' & 'nudoorgeven' werken beide na het verhuisformulier. 
> Als Gem aangeeft wanneer de verhuizing doorgegeven moet worden, vragen mensen soms door met nieuwe datum-gerelateerde vragen. Bijvoorbeeld 'kan het vandaag' of 'kan dat niet nu?'. Gem kan niet zó ver met ze mee rekenen, maar kan nu in dergelijke gevallen wél het algemene antwoord geven ter bevestiging (28 dagen voor - 5 dagen na). 
* #780: Stories toegevoegd voor nieuwe intent 'niet-telefonisch'.
> Mensen vragen soms of het niet telefonisch kan. Gem kan dit nu beantwoorden voor verhuizing doorgeven en paspoort/id-kaart aanvragen (hint: dat kan helaas niet).  

### Actions/Forms/Policies
* Kanaal-splitsing toegepast in action_sc_resultaat om rekening te houden met voice.
> Bij niet-getrainde producten en niet-aangesloten gemeenten verwijst Gem zo goed mogelijk naar relevante pagina's op de website van de gemeente, via de Samenwerkende Catalogi. In de tekstchat verschijnt dan een opsomming van klikbare linkjes. Voor voice is die lijst veel te lang en onhandig. Zeker voor gebruikers die geen screen interface hebben; je kunt dan immers niet klikken of tappen. Daarom biedt de voice variant van dit antwoord een verwijzing naar de algemene website van de gemeente.   

### Other
* Dynamische URL's en kosten in plaats van losse landingspagina's. 
> Sommige informatie zoals hyperlinks en kosten voor producten komen meerdere keren voor in verschillende antwoorden van Gem. Er is nu een centrale plek waar deze internetadressen en kosten beheerd kunnen worden. Via <codes> kunnen ze binnen verschillende antwoorden aangeroepen worden. Als ze veranderen, hoeven ze maar op één plek aangepast te worden. Via de codes worden dan automatisch de aangepaste waarden getoond. 
* Voice: Alle URL's worden getransformeerd naar voice-vriendelijke versies.
> Sommige internetadressen zijn erg lang. Voor voice is het dan niet handig om ze in hun geheel voor te lezen. Daarom worden ze voor voice omgezet naar voorlees-vriendelijke internetadressen. 
* Afspraken & Conventies bestand toegevoegd.
> We maken steeds meer afspraken over hoe we omgaan met zaken als naamgeving en plaatsing binnen RASA. Daar hebben we nu een [overzicht](https://gitlab.com/virtuele-gemeente-assistent/gem/-/blob/master/CONTRIBUTING.md) gemaakt dat we regelmatig bijwerken. 
* #647: Snelle fix zodat 'belasting-kwijtschelding' en 'belasting kwijtschelding' beide goed herkend worden.
* Slot waardes 'feature_content' vervangen van True/False naar CMS, NLX en niet_aangesloten. 
> Deelnemende gemeenten kunnen straks kiezen of ze aansluiten via het instap-cms van Gem, of dat ze via een API willen aansluiten, zodat ze de antwoorden in hun eigen cms kunnen beheren. Om dat onderscheid straks te kunnen maken, hebben we nu alvast een aanpassing in de naamgeving van de waarden van feature_content (oftewel: de check of de betreffende gemeente aangesloten is en zo ja, hoe). 

# 26-08-2020
### NLU & stories
* NLU toegevoegd voor nieuwe intent 'telefonisch_doorgeven'.
> Gem kan nu meer vragen herkennen waarmee mensen bedoelen of ze iets (bv. verhuismelding) telefonisch door kunnen geven. 
* #517: NLU & stories toegevoegd voor nieuwe intent 'productgroep_reisdocumenten_kind_naarbuitenland_toestemmingnodig'.
> Gem kan nu antwoord geven op de vraag of je toestemming nodig hebt (bijvoorbeeld van je ex) als je met je kind naar het buitenland gaat. 
* #486: NLU & stories toegevoegd voor nieuwe intent 'product_verhuismelding_inschrijven_woonbestemming'.
> Het bleek dat sommige mensen zich afvragen of ze zich op specifieke plekken mogen inschrijven, bijvoorbeeld boven een zaak. Gem kan daar nu antwoord op geven (woonbestemming). 
* #470: NLU toegevoegd & story aangepast voor nieuwe intent 'product_verhuismelding_uitschrijven_bevestiging'.
> Gem kon al goed antwoord geven op de vraag 'wanneer geef ik mijn verhuizing door'. Toch vroegen mensen daarna soms nog door, bijvoorbeeld 'kan ik het nu al doorgeven?'. Daar kan Gem nu ook adequaat antwoord op geven. 

### Actions/Forms/Policies
* #420: Als gebruiker tevreden is bij het geven van feedback, wordt er eerst gevraagd of hij nog verbeterpunten heeft. (form_feedback)
> Als een gebruiker aangeeft tevreden te zijn, is de vraag 'wat kunnen we verbeteren' een beetje gek. Daarom vraagt Gem nu 'Kunnen we nog iets verbeteren?'. 
* #762: Indien er 2 datums gevonden worden door het formulier 'when_verhuismelding', zal deze de algemene verhuisdatum-utter sturen. 
> *Gem geeft nu een algemeen, en dus altijd kloppend, antwoord als iemand 2 datums noemt in een vraag over verhuizing doorgeven. Bijvoorbeeld: Ik verhuis volgende week zaterdag, kan ik het nu al doorgeven? ('nu al' wordt ook als datum herkend; namelijk als 'vandaag')*

### Other
* Custom output-channels toegestaan voor end-to-end tests.
> Regressietests kunnen nu ook met speciale kanalen omgaan, zoals voice of facebook messenger
* Redis toevoegd waardoor verschillende responses voor één utter kunnen worden gegeven.
> Soms geeft Gem meerdere keren hetzelfde antwoord binnen één dialoog. Dan is een beetje afwisseling wel zo fijn. We kunnen nu meerdere variaties van hetzelfde antwoord invoeren. Nu zal Gem ze in willekeurige volgorde afvuren. In de toekomst is het waarschijnlijk ook mogelijk om in bepaalde situaties ze op een specifieke volgorde te laten uitspreken. Dit kan handig zijn als er iets mis loopt in de dialoog (dan kan Gem een steeds uitgebreidere versie van het antwoord geven in de hoop dat de gebruiker het wel begrijpt) of bij opsommingen en herhalingen (dan kan Gem het antwoord juist steeds korter en bondiger maken). 
* #482: Aanpassing in action_default_ask_affirmation, zodat deze het output channel herkent en een voice-vriendelijke suggestie-utter geeft aan het voice kanaal.
> Als Gem twijfelt, geeft ze suggesties in de vorm van buttons. Buttons worden in voice niet voorgelezen. Daarom worden de suggesties binnen het voice-kanaal op een voorlees-vriendelijke manier opgenomen binnen het antwoord van Gem, zodat ze alsnog uitgesproken kunnen worden. 

# 12-08-2020
### NLU & stories
* #750: NLU & stories toegevoegd voor nieuwe intent 'productgroep_reisdocumenten_aanvragen_wanneer'. 
> *Gem kan nu antwoord geven op vragen zoals 'Wanneer kies ik voor een paspoort en wanneer voor een ID-kaart?' en 'Wanneer moet ik een paspoort hebben?'*
* #751: NLU & stories toegevoegd voor nieuwe intent 'productgroep_reisdocumenten_aanvragen_waarom'. 
> *Gem kan nu antwoord geven op vragen zoals 'Is het nodig om een paspoort of ID-kaart te hebben?'*
* #779: NLU & stories toegevoegd voor nieuwe intent 'openingstijden_livechat'. 
> *Gem kan nu antwoord geven op vragen over de openingstijden van de livechat. Deze verschillen per deelnemende gemeente*

### Actions/Forms/Policies
* #785: Aanpassing in 'action_envoor' waardoor situatie zoals beschreven in issue werkt. 
>*Als de bezoeker een plaatsnaam invult die Gem niet herkent, en de bezoeker geeft daarna alsnog een correcte gemeentenaam op, kan Gem deze correcte gemeentenaam alsnog oppakken en de dialoog vervolgen.*
* #53: Multi-intent 'oepsikbedoel+inform_affirmative' gemaakt om 'oeps ik bedoelde ja' te laten werken. 
>*Als Gem een vraag stelt en de bezoeker antwoordt eerst 'Nee' en daarna 'Ik bedoel ja', kan Gem dit nu ook oppikken en de dialoog vervolgen met 'ja' als antwoord (en vice versa).* 

### Other
* Keraspolicy vervangen door de TEDPolicy. 
>*We willen graag up-to-date blijven :). De TEDPolicy is actueler en kan allerlei mooie nieuwe dingen, die je straks terug kunt lezen in deze changelog.*

# 05-08-2020
## NLU & stories
* #762: 'Kan het ook nu al?' toegevoegd aan intent datum_bevestiging. *Als iemand een datum invult, en Gem geeft antwoord, gebeurt het soms dat ze daarna 'kan het nu ook al' vragen, of iets vergelijkbaars. Daar kan Gem nu mee omgaan.*
* Fout in story voor paspoort_aanvragen verholpen. ('Ik wil een paspoort' moet weer werken). *'Ik wil een paspoort' ging een tijdje niet goed. Die bug is nu gefixed.*
* NLU voor diverse intents toegevoegd. *We hebben weer zinnen (training phrases) toegevoegd die bezoekers zoal intypen, zodat Gem die beter kan herkennen en er goed antwoord op kan geven.*

## Actions/Forms/Policies
* Action_default_core aangepast: Gem zou niet meer stil moeten blijven als ze iets niet begrijpt. *Soms bleef Gem stil als ze een vraag niet goed begreep. Dat is nu verholpen.*
* Verdiepingspolicy aangepast naar nieuwe utternaam-conventie. Als het een lokale utter betreft wordt naar gemeente gevraagd indien dat onbekend is. *Als bezoekers doorvragen, bijvoorbeeld 'wat kost dat dan' of 'waar kan ik dat doen', moet Gem rekening kunnen houden met het feit dat sommige antwoorden verschillen per gemeente. Als het antwoord inderdaad verschilt per gemeente, zal Gem eerst kijken of het de gemeente al weet. Zo niet, dan vraagt Gem daar eerst naar. Dit werkte al, maar de manier van herkennen van lokaal aanpasbare antwoorden is nu aangepast naar de nieuwe naamgeving (_lo & _la).*
* #53: multi-intent 'oepsikbedoel+inform_denial' gemaakt om 'oeps ik bedoelde nee' te laten werken. *Als Gem een vraag stelt en de bezoeker antwoordt eerst 'Ja' en daarna 'Ik bedoel nee', kan Gem dit nu ook oppikken en de dialoog vervolgen met 'nee' als antwoord.* 

## Other
* Utternamen gewijzigd zodat gezien kan worden welke lokaal en welke landelijk zijn (_lo & _la). *Het is belangrijk dat direct duidelijk is welke antwoorden verschillen per gemeente (lokaal) en welke in alle gemeenten hetzelfde zijn (landelijk). Dit hebben we nu duidelijk gemaakt in de naamgeving: _lo voor lokaal aanpasbare antwoorden en _la voor landelijke antwoorden.*


# 29-07-2020
## NLU & stories
* #723: NLU & stories toegevoegd voor nieuwe intent 'productgroep_reisdocumenten_evenduur'. *Gem kan nu antwoord geven op vragen als 'is een paspoort overal even duur?*
* #749: 'afgifte bewijs' toegevoegd aan NLU 'afhaalbewijs'. *Gem herkent 'afgifte bewijs' nu als afhaalbewijs*
* #470: NLU & stories toegevoegd voor nieuwe intent 'product_verhuismelding_uitschrijven'. *Gem kan nu antwoord geven op vragen zoals 'wanneer moet ik me uitschrijven bij de gemeente' en 'ik ga weg uit [gemeentenaam]*
* #704: NLU & stories toegevoegd voor nieuwe intent 'openingstijden_gem'. *Gem kan nu antwoord geven op de vraag wanneer Gem (de chatbot) bereikbaar is.*
* #688: NLU toegevoegd voor intent 'overig_foto_hoeveel'. *Gem kan nu meer bezoekersvragen herkennen rondom de vraag hoeveel foto's nodig zijn (bv. bij paspoort aanvraag).*
* #539: NLU & stories toegevoegd voor nieuwe intent 'product_paspoort_aanvragen_tweede'. *Gem kan nu antwoord geven als iemand een tweede paspoort wil aanvragen.*
* #774: Oversampling uitgevoerd voor 'inform_affirmative' ('ja'), zodat 'ja' altijd als inform_affirmative herkend wordt. *'Ja' werd niet altijd herkend als 'ja'. Nu gaat dat beter.*
* #770: Mergen van intents 'productgroep_reisdocumenten_aanvragen_watmeenemen', 'productgroep_reisdocumenten_kind_watmeenemen' & 'watmeenemen'. *Als bezoekersvragen veel op elkaar lijken, kun je ze beter samenvoegen, zodat Gem ze beter kan herkennen. Dat hebben we bij deze gedaan.*

## Actions/Forms/Policies
* Aanpassing binnen form_verhuismelding_when: contentAPI eruit gehaald.

## Other
* #771: 'Broken links checker' toegevoegd aan end2end container. *We kunnen nu regelmatig alle hyperlinks waarnaar Gem verwijst, automatisch controleren of ze nog werken. Wel zo handig omdat internetadressen van webpagina's soms veranderen.*
* #435: Tekstwolk + animatie toegevoegd aan widget. *De chatwidget valt niet altijd even goed op binnen een webpagina. Daarom verschijnt er nu enkele seconden een extra tekstwolkje "chat met gem" naast de chatwidget.*
* #513 Voice: Indien Gem 3 responses geeft, worden response 2 & 3 gemerged tot één response. *Google Voice geeft maximaal 2 antwoorden, maar Gem heeft soms 3 tot 4 antwoorden. Daarom worden deze voor voice samengevoegd zodat Gem ook daar het volledige antwoord geeft.*
* #381 Config: 'inform_denial' ingesteld als 'deny_suggestion_intent_name'. Indien iemand 'nee' antwoordt op gekregen suggesties moet dit werken. *Als een bezoeker 'Nee' zegt na suggesties van Gem ('Bedoel je... [suggestie 1] [suggestie 2]') zal Gem nu vragen of je het ook op een andere manier kunt vragen.*


# 22-07-2020
## NLU & stories
* #678: NLU & stories toegevoegd voor nieuwe intent 'kind_naarbuitenland_metanderen'. *Gem kan nu antwoord geven wanneer iemand op reis gaat met een kind van een ander (bijvoorbeeld een vriendje of kleinkind).*
* #488: NLU & stories toegevoegd voor nieuwe intent 'productgroep_reisdocumenten_verlengen_wanneer'. *Gem kan nu antwoord geven op de vraag 'wanneer moet ik mijn paspoort verlengen' of 'wanneer verloopt mijn identiteitskaart'.*
* #462: NLU & stories toegevoegd voor nieuwe intent 'overig_foto_waar'. *Gem kan nu antwoord geven op de vraag of je een pasfoto kunt laten maken bij de gemeente.*
* #727: NLU & stories toegevoegd voor nieuwe intent 'compliment_uitgebreid'. *Soms krijgt Gem grote complimenten, zoals bijvoorbeeld 'wat weet je toch veel' of 'wat ben je toch slim'. Hoe leuk is dat. Gem kan die nu passend in ontvangst nemen.*
* #536: NLU & stories toegevoegd voor nieuwe intent 'product_verhuismelding_kind_inschrijven_waar'. *Gem krijgt soms vragen over waar een kind ingeschreven moet staan, bijvoorbeeld na een echtscheiding. Gem kan daar nu antwoord op geven.*

## Actions/Forms/Policies
* #689: Tijd voor action_helpmore gereduceerd naar 45 seconden. *Voor de makers van Gem is feedback belangrijk, zodat we Gem steeds beter kunnen maken. Daarom komt de 'mening geven' knop nu iets eerder in beeld als de bezoeker even niets intypt.*
* #689: Hamburger menu toegevoegd aan widget. *We willen een aantal mogelijkheden continu in beeld hebben, zoals feedback geven, het gesprek downloaden en het privacy statement bekijken. Via een hamburger-menu (3 horizontale streepjes) zijn die mogelijkheden nu continu in beeld. Het liefste zouden we hier ook 'livechat' aan toevoegen, maar dat kan nu nog niet i.v.m. openingstijden livechat.*

## Other
* RASA versie 1.10.2. Bij testen let op: is intent-classificatie verbeterd? + Worden synoniemen & woonplaatsen nog steeds herkend? *We hebben onze software ge-updated en dan testen we natuurlijk stevig of alles goed blijft gaan.*
* Voice: buttons + titels van URL's worden uitgesproken. (Niet testbaar in widget).

# 15-07-2020
## NLU & stories
* Spellchecker toegevoegd. Bij testen let op: spellingsfouten m.b.t. product & municipality.
* Verdiepingsintents 'digitaal' & 'hier' toegevoegd.
* #433: greet_back stories + kleine aanpassing in verdiepingspolicy.
* Fix in story 'jijook'.
* #715: entity 'namens' toegevoegd aan intent 'verhuismelding_doorgeven_wanneer' + bijbehorende NLU.
* NLU & stories toegevoegd voor nieuwe intent 'productgroep_reisdocumenten_dubbelenationaliteit_algemeen'.
* #712: NLU & stories toegevoegd voor nieuwe intent 'datum_bevestiging'.
* NLU toegevoegd voor nieuwe intent 'wanneernodig'.
* #679: Stories gewijzigd.

## Actions/Forms/Policies
* #748: update in verhuisformulier, stuurt algemene utter als 'grain' niet gevonden kan worden in entity time.

## Other


# 08-07-2020
## NLU & stories
* #538: NLU & stories toegevoegd voor nieuwe intent 'burgerservicenummer_info'.
* #688: NLU & stories toegevoegd voor nieuwe intent 'overig_foto_hoeveel'.
* NLU & stories toegevoegd voor nieuwe intent 'hoeheetjij'.
* NLU & stories toegevoegd voor nieuwe intents 'benjijsinge' & 'hebjijeenpartner'.
* NLU & stories toegevoegd voor nieuwe intent 'hoeoudbenjij'.
* NLU & stories toegevoegd voor nieuwe intent 'wieheeftjougemaakt'.
* NLU & stories toegevoegd voor nieuwe intent 'downloadtoestemmingsformulier'.
* NLU & stories toegevoegd voor nieuwe intent 'toestemmingnodig'.
* NLU & stories toegevoegd voor nieuwe intent 'geentoestemming'.
* #504: Entity 'met spoed' toegevoegd aan NLU van intents 'oepsikbedoel' en 'envoor'.

## Actions/Forms/Policies
* Prioriteit van verdiepingspolicy teruggezet van plaats 7 naar plaats 5.
* #393: Verdiepingspolicy uitgebreid met check op gemeente.
* 'Out-of-Scope' action & policy toegevoegd met prioriteit 2. Dit zorgt ervoor dat Keras geen foutieve voorspellingen meer maakt met niet-getrainde producten. In plaats daarvan wordt gebruiker direct doorgeleid naar samenwerkende catalogi.

## Other
* 'Queries' toegevoegd aan CMS.
