Afspraken en conventies Gem in Rasa
=========

## INTENTS

### Bij het aanmaken van nieuwe intents
- suggestie maken in domain.yml
- deze suggestie óók toevoegen als training phrase bij nlu (voor als voice suggesties voorleest en klant deze na-spreekt)
- in domain.yml standaard ignore_entities: [namens] toevoegen, *tenzij* de namens-entiteit een verschil maakt in het verloop van de story 

## UTTERS / RESPONSES

### Naamgeving
- vragen die gem stelt beginnen met utter_ask
- landelijke antwoorden beginnen met utter_la_
- lokaal aanpasbare antwoorden beginnen met utter_lo_
- productgerelateerde antwoorden hebben na _lo_ of _la_ het woord _product_ (verhuismelding) of _productgroep_ (paspoort of id-kaart)

### Locaties
- lokaal aanpasbare antwoorden worden zowel in responses.yml opgenomen (de 'landelijke' utters; hier wordt de default fallback tekst opgenomen) als in local.yml (de lokale utters; hier worden per aangesloten gemeente de lokale utters opgenomen)
- iedere utter staat óók 2x in domain.yml; bovenaan in de utters-lijst en onderaan bij responses met slechts : [] erachter, zodat validate probleemloos kan blijven draaien. 

### Lokale utters/responses
- in local.yml worden alle lokale varianten van aangesloten gemeenten opgenomen
- per gemeente een - text: [hier het lokale antwoord]
- daarnaast een set metadata: 
> - example: [hier een voorbeeldzin zodat gemeenten altijd een correct voorbeeld zien, ook als ze de utter al ingrijpend gewijzigd hebben]
> - helptext: [hier een uitleg voor bij het aanpassen; bijvoorbeeld hoe te werken met hyperlinks, witregels etc. maar ook aanwijzingen t.a.v. de inhoud]
> - questions: [de vraag/vragen waarop deze utter/response een antwoord is]
> - required: yes [voor utters die per gemeente aangepast *moeten* worden, zoals bv. openingstijden]
> - related: [hier indien nodig de utter(s) die inhoudelijk zeer verwant zijn; zo kunnen gemeenten zien dat als ze deze aanpassen, ze de andere ook moeten controleren]

### Buttons en voice
- voice kan handmatig gemaakte buttons niet voorlezen
- de labels van buttons zijn binnen gelezen context (chatvenster) wel logisch, maar binnen gesproken context (voice) niet; daarom wordt er ook niets gecreëerd om buttons alsnog uit te laten spreken
- uitgangspunt: handmatige buttons worden alleen als 'extraatje' aangeboden; utters moeten zelfstandig kunnen functioneren zonder handmatige buttons
- alleen bij uitzonderingen waar buttons een noodzakelijk element van de utter zijn, wordt voor voice een aangepaste utter gemaakt



