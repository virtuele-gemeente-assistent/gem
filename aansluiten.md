# Als gemeente aansluiten op Gem

Om als gemeente aan te sluiten op Gem, enkele stappen nodig voor de inhoud, de techniek, styling en het proces. 

## Inhoud

- Vul de [basisvragenlijst](https://teams.microsoft.com/l/file/65E64A6A-A54C-412D-A9B1-69051734ED0D?tenantId=6ef029ab-3fd7-4d98-9b0e-d1f5fedea6d1&fileType=dotx&objectUrl=https%3A%2F%2Fvvng.sharepoint.com%2Fsites%2FFrontofficevandetoekomst%2FGedeelde%20documenten%2FKanaalonafhankelijk%20content%2FNieuwe%20gemeente%20aansluiten%2FBasis%20vragenlijst%20voor%20nieuwe%20deelnemende%20gemeenten%20-%20voor%20inhoudelijke%20antwoorden%20Gem.dotx&baseUrl=https%3A%2F%2Fvvng.sharepoint.com%2Fsites%2FFrontofficevandetoekomst&serviceName=teams&threadId=19:7d9b4113319d405490a70e3f1d3123ce@thread.skype&groupId=21545b2d-83fa-4b06-9862-4f001d5308b1) in voor jouw gemeente, en stuur die terug naar het Gem team. Daarmee vullen we de meest basale informatie voor jouw gemeente in. 
- Daarna krijg je een overzicht van alle antwoorden die Gem geeft namens jouw gemeente. Deze kun je nalopen om te zien of het klopt, en waar nodig aanpassen. Het aanpassen kun je zelf in het CMS doen als je daar voor kiest. 

## Techniek

- Zodra Gem inhoudelijk is ingevuld (zie boven) kan het widget geplaatst worden op de website van je gemeente. Hiervoor moet je een code aanmaken met de [code generator](https://teams.microsoft.com/l/file/3B27AFE0-412B-4A9C-9D6D-59F5375F0AB4?tenantId=6ef029ab-3fd7-4d98-9b0e-d1f5fedea6d1&fileType=xlsx&objectUrl=https%3A%2F%2Fvvng.sharepoint.com%2Fsites%2FFrontofficevandetoekomst%2FGedeelde%20documenten%2FKanaalonafhankelijk%20content%2FNieuwe%20gemeente%20aansluiten%2FGem%20Widget%20Codemaker.xlsx&baseUrl=https%3A%2F%2Fvvng.sharepoint.com%2Fsites%2FFrontofficevandetoekomst&serviceName=teams&threadId=19:7d9b4113319d405490a70e3f1d3123ce@thread.skype&groupId=21545b2d-83fa-4b06-9862-4f001d5308b1). 
- De code die je hebt aangemaakt, lever je aan bij je webontwikkelaar. Geef daarbij duidelijk aan op welke webpagina's Gem moet verschijnen; op die pagina's moeten de codes geplaatst worden. 

## Styling
- Lever het logo van jouw gemeente aan, liefst met een transparante achtergrond. 
- Als je gebruik maakt van een koppeling met livechat (Obi4Wan), lever dan ook een png-versie van 30x30px van het logo van jouw gemeente aan, liefst met een transparante achtergrond, voor gebruik op een witte achtergrond. 
- Lever de hoofdkleur van je gemeentelijke huisstijl aan, bijvoorbeeld als [hex code](https://www.color-hex.com/).

## Proces

- Lees het [stappenplan](#.doc) voor het klaarmaken van jouw gemeente en jouw klantcontactcentrum voor werken met Gem. 
- Werk je ook met livechat via Gem, lees dan het [stappenplan gem met livechat](#.doc).


