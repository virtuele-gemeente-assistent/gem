
NB: Deze pagina komt te vervallen. De nieuwe uitlegpagina is: https://gitlab.com/virtuele-gemeente-assistent/gem/-/wikis/Codes-in-AntwoordenCMS

_____


# Uitleg bij codes in CMS

[TOC]

### Code voor hyperlink uit CMS

De code voor het invoeren van een lokale hyperlink uit de hyperlinks-tegel van het CMS herken je als volgt: 

` [<pagina_contact.label>](<pagina_contact.url>) ` 

<i>In dit voorbeeld de contactpagina. In het eerste gedeelte tussen [<>] wordt de klikbare tekst (label) opgehaald uit 'Hyperlinks en Leges' in het CMS. In het tweede gedeelte tussen (<>) wordt het internetadres opgehaald, waar naartoe gelinkt moet worden. Om dubbel onderhoudswerk te voorkomen, kun je deze code het beste ongewijzigd laten. Klopt de link niet? Pas deze dan aan via de tegel 'Hyperlinks' in het CMS. </i>

### Handmatig een hyperlink toevoegen 

Soms is het handig om handmatig een hyperlink toe te voegen. Bijvoorbeeld als jouw informatie op 2 pagina's staat in plaats van 1. Dit doe je met de volgende notatie: 

`[Contactpagina](https://www.witterland.nl/contact)`. 

*Tussen de `[`vierkante haken`]` staat de klikbare tekst en tussen de `(`ronde haken`)` het internetadres. **NB:** Zorg dat er géén spatie tussen het 2e vierkante haakje en het 1e ronde haakje staat.* 

### Witregel toevoegen 

Je kunt witregels maken door 2x enter te geven. 

### Bullets toevoegen 

Je kunt bullets maken met streepjes: 

````
- bullet 1
- bullet 2
- etc.
````

### Tekst cursief of vet maken

Je kunt iets `*cursief*` maken met enkele sterretjes, en `**vet**` met dubbele sterretjes. Let op dat er géén spaties zitten tussen de sterretjes en de tekst; anders werkt het niet. 

### Wat betekent {product}?

` {product} ` : Hiermee wordt het product getoond dat Gem op dat moment in haar geheugen heeft. Bijvoorbeeld een paspoort of een ID-kaart. Als je dit tegen komt in een antwoord, dan kun je dit het beste laten staan, zodat het antwoord van Gem altijd klopt met het product waar het op dat moment over gaat. 


